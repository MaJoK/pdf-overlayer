/*  PDF-Overlayer, a GUI application utilizing JavaFx to overlay several PDF Files
    Copyright (C) 2018  Mark Joachim Krallmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.gitlab.majok.pdfOverlayer;

import java.awt.Desktop;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gitlab.majok.pdfOverlayer.OverlayProfile.ConstructException;
import com.gitlab.majok.pdfOverlayer.modifedOverlayPDFTool.ModifiedOverlayPDF;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * The class meant to be used as a program entry, this class also manages the
 * pdf-overlayer window.
 * 
 *
 */
public class PDFOverlayerEntry extends Application {

	/**
	 * Keys to be used to store program information under.
	 */
	static final String profilesKey = "profiles", licenseShownKey = "licenseShown";

	public static void main(String[] args) {
		Application.launch(args);
	}

	/**
	 * The globally last selected File in a FileChooser,
	 */
	File lastSelected;

	/**
	 * Whether the license has been shown to the user.
	 */
	boolean licenseShown = false;

	/**
	 * TextField of the input File
	 */
	TextField inputPathTextField;

	/**
	 * TextField of the output File
	 */
	TextField outputPathTextField;

	/**
	 * Stage of the primary pdf-overlayer window.
	 */
	Stage primaryStage;

	ListView<OverlayProfile> profileListView;

	/**
	 * Label to present the user the success or failure of an overlaying operation
	 */
	private Label statusLabel;
	/**
	 * The last File that was successfully overlayed
	 */
	private File successfullOutputFile;

	private Button openFileFolder, openFile;

	boolean currentlySettingBottomField = false;
	Object currentlySettingBottomField_syncObject = new Object();
	/**
	 * Global list to store the profiles in.
	 */
	ObservableList<OverlayProfile> profileList;

	boolean userManuallyChangedBottomFile = false;

	@Override
	public void start(Stage primaryStage) throws Exception {
		profileList = FXCollections.observableArrayList();
		// Reading and loading the data from the config file, this includes the
		// profiles.
		readConfigFile();

		boolean showLicense = false;
		if (licenseShown == false) {
			licenseShown = true;
			showLicense = true;
		}

		this.primaryStage = primaryStage;

		assembleMainWindow();

		primaryStage.setTitle(GUIStrings.mainWindow.mainWindowTitle);

		primaryStage.show();

		
		if (showLicense) {
			openAboutWindow();
			// We need to save it immediately if the license has been shown.
			writeOutConfigFile();
		}

	}

	void addProfile(OverlayProfile profile) {
		profileList.add(profile);
		writeOutConfigFile();
	}

	/**
	 * Assembles the main Window and puts the content into the primary Stage.
	 */
	private void assembleMainWindow() {
		GridPane gp = new GridPane();
		gp.setPadding(new Insets(10));
		gp.setVgap(10);
		gp.setHgap(5);

		ColumnConstraints cc = new ColumnConstraints();
		cc.setHgrow(Priority.ALWAYS);
		cc.setPrefWidth(350);
		gp.getColumnConstraints().add(0, cc);
		ColumnConstraints cc2 = new ColumnConstraints();
		cc2.setMaxWidth(ColumnConstraints.CONSTRAIN_TO_PREF);
		gp.getColumnConstraints().add(1, cc2);

		gp.setMaxWidth(Double.MAX_VALUE);
		Scene scene = new Scene(gp);
		primaryStage.setScene(scene);

		assembleTopPanel(gp);
		assembleMiddlePanel(gp);
		assembleBottomPanel(gp);
		scene.setOnDragOver(dragEvent -> {
			if (dragEvent.getDragboard().hasFiles()) {
				Dragboard dragboard = dragEvent.getDragboard();
				List<File> files = dragboard.getFiles();
				if (files.get(0).getPath().toUpperCase().endsWith(".PDF"))
					dragEvent.acceptTransferModes(TransferMode.ANY);
				dragEvent.consume();
			}

		});
		scene.setOnDragDropped(dragEvent -> {
			if (dragEvent.getDragboard().hasFiles()) {
				Dragboard dragboard = dragEvent.getDragboard();
				List<File> files = dragboard.getFiles();
				inputPathTextField.setText(files.get(0).getPath());
				dragEvent.consume();
			}
		});
	}

	// The Top Panel contains a TextField which shows the Inputfile, a
	// Label showing that it's the input file and a button opening a file chooser.
	private void assembleTopPanel(GridPane gp) {

		Label inputFileLabel = new Label();
		inputFileLabel.setText("Inputfile:");
		inputFileLabel.setAlignment(Pos.CENTER_LEFT);
		gp.add(inputFileLabel, 0, 0, 1, 1);

		inputPathTextField = new TextField();
		inputPathTextField.textProperty().addListener((value, oldValue, newValue) -> {
			updateOutputFile();
		});
		gp.add(inputPathTextField, 0, 1);

		Hyperlink about = new Hyperlink(GUIStrings.mainWindow.about);
		about.setOnAction(ae -> openAboutWindow());
		gp.add(about, 1, 0);
		GridPane.setHalignment(about, HPos.RIGHT);
		GridPane.setValignment(about, VPos.TOP);
		Button fileChooseStart = getFileOpenButton(inputPathTextField, primaryStage);
		EventHandler<ActionEvent> backUpChooser = fileChooseStart.getOnAction();
		fileChooseStart.setOnAction(actionEvent -> {
			OverlayProfile selProfile = profileListView.getSelectionModel().getSelectedItem();
			if (selProfile != null && selProfile.standardIn != null) {
				FileChooser fc = new FileChooser();
				fc.getExtensionFilters()
						.add(new ExtensionFilter(GUIStrings.fileChooserPDFFilesExtensionLabel, "*.pdf"));
				File initialDir = new File(selProfile.standardIn);
				if (initialDir.exists() && initialDir.isDirectory())
					fc.setInitialDirectory(initialDir);
				File selected = fc.showOpenDialog(primaryStage);

				if (selected != null) {
					lastSelected = selected;
					inputPathTextField.setText(selected.getPath());
					inputPathTextField.positionCaret(selected.getPath().length());
				}
			} else
				backUpChooser.handle(actionEvent);
		});
		gp.add(fileChooseStart, 1, 1);
	}

	private void updateOutputFile() {
		OverlayProfile selected = profileListView.getSelectionModel().getSelectedItem();
		if (selected != null && selected.standardOut != null && !userManuallyChangedBottomFile) {
			Path input = Paths.get(inputPathTextField.getText());
			Path out = Paths.get(selected.standardOut, input.getFileName().toString());
			currentlySettingBottomField = true;
			outputPathTextField.setText(out.toString());
			currentlySettingBottomField = false;
		}
		// Hiding the last status
		statusLabel.setText("");
		openFile.setVisible(false);
		openFileFolder.setVisible(false);
	}

	private void assembleMiddlePanel(GridPane gp) {

		profileListView = new ListView<OverlayProfile>(profileList);

		Label placeholder = new Label("Created profiles will be shown here");
		placeholder.setWrapText(true);
		placeholder.setAlignment(Pos.CENTER);
		profileListView.setPlaceholder(placeholder);
		profileListView.setPrefHeight(200);
		profileListView.setMaxHeight(Double.MAX_VALUE);
		gp.add(profileListView, 0, 2, 1, 5);

		profileListView.getSelectionModel().selectedItemProperty().addListener((value, old, newVal) -> {
			if (newVal != null) {
				updateOutputFile();
			}
		});
		GridPane.setVgrow(profileListView, Priority.SOMETIMES);
		Button newProfile = new Button(GUIStrings.mainWindow.newProfileButton);
		newProfile.setOnAction(ae -> {
			new ProfileEditor(this);
		});
		Button editProfile = new Button(GUIStrings.mainWindow.editProfileButton);
		editProfile.setOnAction(ae -> {
			int selectedIndex = profileListView.getSelectionModel().getSelectedIndex();
			if (selectedIndex >= 0) {
				new ProfileEditor(profileList.get(selectedIndex), this);
			}
		});
		Button removeProfile = new Button(GUIStrings.mainWindow.removeProfileButton);
		removeProfile.setOnAction(ae -> {
			int selectedIndex = profileListView.getSelectionModel().getSelectedIndex();
			if (selectedIndex >= 0) {
				Alert conformation = new Alert(AlertType.CONFIRMATION);
				conformation.setContentText(GUIStrings.mainWindow.conformationDeleteProfile);
				conformation.initOwner(primaryStage);
				conformation.initModality(Modality.APPLICATION_MODAL);
				conformation.showAndWait().ifPresent(buttonType -> {
					if (buttonType == ButtonType.OK) {
						profileList.remove(selectedIndex);
						writeOutConfigFile();
					}
				});

			}
		});
		Button suggestInputFile = new Button(GUIStrings.mainWindow.suggestInputFile);
		Tooltip suggestInputFileTooltip = new Tooltip(GUIStrings.mainWindow.suggestInputFileTooltip);
		Tooltip.install(suggestInputFile, suggestInputFileTooltip);

		suggestInputFile.setOnAction(ae -> {
			selectLatestPDFFileAsInput();
		});
		newProfile.setMaxWidth(Double.MAX_VALUE);
		editProfile.setMaxWidth(Double.MAX_VALUE);
		removeProfile.setMaxWidth(Double.MAX_VALUE);
		suggestInputFile.setMaxWidth(Double.MAX_VALUE);

		gp.add(newProfile, 1, 2);
		gp.add(editProfile, 1, 3);
		gp.add(removeProfile, 1, 4);
		gp.add(suggestInputFile, 1, 5);

		GridPane.setHgrow(newProfile, Priority.SOMETIMES);
		GridPane.setHgrow(editProfile, Priority.SOMETIMES);
		GridPane.setHgrow(removeProfile, Priority.SOMETIMES);
		GridPane.setHgrow(suggestInputFile, Priority.SOMETIMES);

		newProfile.setMinWidth(Button.USE_PREF_SIZE);
		editProfile.setMinWidth(Button.USE_PREF_SIZE);
		removeProfile.setMinWidth(Button.USE_PREF_SIZE);
		suggestInputFile.setMinWidth(Button.USE_PREF_SIZE);

		profileListView.getSelectionModel().selectedItemProperty().addListener((value, old, newVal) -> {
			boolean disableButtons = newVal == null;
			editProfile.setDisable(disableButtons);
			removeProfile.setDisable(disableButtons);
			// Is there a standardin selected?
			suggestInputFile.setDisable(!(newVal != null && newVal.standardIn != null));
		});

		editProfile.setDisable(true);
		removeProfile.setDisable(true);
		suggestInputFile.setDisable(true);
	}

	private void selectLatestPDFFileAsInput() {
		OverlayProfile p = profileListView.getSelectionModel().getSelectedItem();
		if (p == null || p.standardIn == null)
			return;

		Path inDir = Paths.get(p.standardIn);
		if (!Files.exists(inDir))
			return;

		try {
			Tuple<Path, Instant> latestFile = new Tuple<Path, Instant>();
			// firstField is the actual path, secondField is the latestModifaction time
			Files.list(inDir).filter(Files::isRegularFile)
					.filter(file -> file.getFileName().toString().toUpperCase().endsWith(".PDF")).forEach(file -> {
						FileTime lastMod;
						try {
							lastMod = Files.getLastModifiedTime(file);
							if (latestFile.secondField == null
									|| latestFile.secondField.isBefore(lastMod.toInstant())) {

								latestFile.firstField = file;
								latestFile.secondField = lastMod.toInstant();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
			if (latestFile.firstField != null) {
				inputPathTextField.setText(latestFile.firstField.toString());
			}
		} catch (IOException e) {
			// TODO graphical Error
			e.printStackTrace();
		}
	}

	private final int bottomPanelStartRowIndex = 7;

	private void assembleBottomPanel(GridPane gp) {

		Label label = new Label();
		label.setText("Outputfile:");
		label.setAlignment(Pos.CENTER_LEFT);

		gp.add(label, 0, bottomPanelStartRowIndex);

		outputPathTextField = new TextField();
		gp.add(outputPathTextField, 0, bottomPanelStartRowIndex + 1);

		Button fileChooseStart = new Button("...");
		fileChooseStart.setOnAction(ae -> {
			FileChooser fc = new FileChooser();
			fc.setTitle("Select the PDF File");
			fc.getExtensionFilters().add(new ExtensionFilter("PDF files", "*.pdf"));

			if (lastSelected == null || lastSelected.getParentFile() == null) {
				// Showing the initial entered file
				String enteredFilePath = inputPathTextField.getText();
				if (!enteredFilePath.isEmpty()) {
					File file = new File(enteredFilePath);
					String fileName = file.getName();
					fc.setInitialFileName(fileName);
					fc.setInitialDirectory(new File(file.getParent()));
				}
			} else {
				// Showing the last entered File
				String fileName = lastSelected.getName();
				File dir = lastSelected.getParentFile();
				fc.setInitialFileName(fileName);
				fc.setInitialDirectory(dir);
			}
			File selected = fc.showSaveDialog(primaryStage);
			if (selected != null) {
				synchronized (currentlySettingBottomField_syncObject) {
					currentlySettingBottomField = true;
					outputPathTextField.setText(selected.getPath());
					outputPathTextField.positionCaret(selected.getPath().length() - 1);
					currentlySettingBottomField = false;
				}
				lastSelected = selected;
			}
		});

		outputPathTextField.textProperty().addListener((source, old, newVal) -> {

			synchronized (currentlySettingBottomField_syncObject) {
				// If the program sets the output field the user didn't and vice versa
				userManuallyChangedBottomFile = !currentlySettingBottomField;
			}

		});

		gp.add(fileChooseStart, 1, bottomPanelStartRowIndex + 1);

		statusLabel = new Label();
		gp.add(statusLabel, 0, bottomPanelStartRowIndex + 2, 1, 2);
		statusLabel.setWrapText(true);

		openFile = new Button(GUIStrings.mainWindow.openFileButton);
		openFile.setOnAction(this::openFileMethod);
		openFile.setMaxWidth(Double.MAX_VALUE);
		gp.add(openFile, 1, bottomPanelStartRowIndex + 2);
		openFile.setVisible(false);

		openFileFolder = new Button(GUIStrings.mainWindow.openFolderButton);
		openFileFolder.setOnAction(this::openFolderMethod);
		openFileFolder.setMaxWidth(Double.MAX_VALUE);
		gp.add(openFileFolder, 1, bottomPanelStartRowIndex + 3);
		openFileFolder.setVisible(false);

		GridPane.setHgrow(openFile, Priority.ALWAYS);
		GridPane.setHgrow(openFileFolder, Priority.ALWAYS);
		Button startButton = new Button(GUIStrings.mainWindow.startProcess);

		startButton.setOnAction(ae -> {
			int selected = profileListView.getSelectionModel().getSelectedIndex();
			if (selected != -1) {
				runProfile(profileList.get(selected));
			} else {
				statusLabel.setText(GUIStrings.mainWindow.errNeedToSelectProfile);
				statusLabel.setTextFill(Color.DARKGOLDENROD);
			}
		});
		startButton.setMaxWidth(Double.MAX_VALUE);
		GridPane.setHgrow(startButton, Priority.ALWAYS);
		gp.add(startButton, 0, bottomPanelStartRowIndex + 4, 2, 1);
	}

	public String getDataDirectory() {
		String os = System.getProperty("os.name").toUpperCase();
		File dataFolder;
		if (os.contains("WIN")) {
			dataFolder = new File(System.getenv("AppData"));
		} else {
			dataFolder = new File(System.getenv("HOME") + "/.local/share");
		}
		File folder = new File(dataFolder.getPath() + "/mjk-pdf-overlayer/");
		folder.mkdirs();
		return folder.getPath() + "/";
	}

	/**
	 * Returns a preconfigured Button which opens a FileChooser and has the initial
	 * directory set to the directoy of the file entered in the TextField, if the
	 * TextField is empty or contains no valid file it will use the directory of the
	 * {@link #lastSelected} File-field of this {@link PDFOverlayerEntry} instance.
	 * 
	 * After a file has been selected it will update the lastSelected field.
	 * 
	 * @param pathField the TextField to write the selected filepath into.
	 * @return
	 */
	public Button getFileOpenButton(TextField pathField, Window owner) {
		Button button = new Button("...");
		button.setOnAction(ae -> {

			boolean enteredPathIsValid = true;

			if (pathField.getText().isEmpty()) {
				enteredPathIsValid = false;
			} else {
				File enteredFile = new File(pathField.getText());
				if (!enteredFile.exists())
					enteredPathIsValid = false;
			}

			File toWorkWith;
			if (enteredPathIsValid) {
				toWorkWith = new File(pathField.getText());
			} else {
				toWorkWith = lastSelected;
			}

			FileChooser fc = new FileChooser();
			// If no file has been selected before lastSelected is null and this reference
			// will be too.
			if (toWorkWith != null) {
				if (toWorkWith.isFile()) {
					fc.setInitialDirectory(toWorkWith.getParentFile());
					fc.setInitialFileName(toWorkWith.getName());
				} else {
					fc.setInitialDirectory(toWorkWith);
				}
			}

			fc.getExtensionFilters().add(new ExtensionFilter(GUIStrings.fileChooserPDFFilesExtensionLabel, "*.pdf"));
			File selected = fc.showOpenDialog(owner);
			if (selected != null) {
				lastSelected = selected;
				pathField.setText(selected.getPath());
				pathField.positionCaret(selected.getPath().length());
			}
		});
		return button;
	}

	private void openFileMethod(ActionEvent ae) {
		// Otherwise the Applications hangs itself up
		new Thread(() -> {
			try {
				Desktop.getDesktop().open(successfullOutputFile.getAbsoluteFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
	}

	private void openFolderMethod(ActionEvent ae) {

		// Otherwise the Application hangs itself up
		new Thread(() -> {
			try {
				if (successfullOutputFile.getParentFile().exists()) {
					Desktop.getDesktop().open(successfullOutputFile.getParentFile().getAbsoluteFile());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}).start();
	}

	private void readConfigFile() {
		File dataFile = new File(getDataDirectory() + "pdf-overlayer_saved-data.json");
		if (dataFile.exists()) {
			JSONParser parser = new JSONParser();
			try (FileReader fr = new FileReader(dataFile)) {
				JSONObject configFile = (JSONObject) parser.parse(fr);
				JSONArray profilesArr = (JSONArray) configFile.get(profilesKey);
				for (Object o : profilesArr) {
					profileList.add(new OverlayProfile((JSONObject) o));
				}
				licenseShown = (Boolean) configFile.get(licenseShownKey);
			} catch (IOException e) {
				// TODO show graphical error
				e.printStackTrace();
			} catch (NullPointerException | ParseException | ConstructException e) {
				// TODO show graphical error and ask whether to delete the config file
				e.printStackTrace();
			}
		}
	}

	void runProfile(OverlayProfile profile) {
		if (userManuallyChangedBottomFile
				&& inputPathTextField.getText().trim().equals(outputPathTextField.getText().trim())) {
			Alert alert = new Alert(AlertType.CONFIRMATION, GUIStrings.mainWindow.warnWillOverwriteInputFile);
			alert.initOwner(primaryStage);
			alert.initModality(Modality.APPLICATION_MODAL);
			Optional<ButtonType> pressed = alert.showAndWait();
			if (pressed.isPresent() == false || pressed.get() != ButtonType.OK) {
				return;
			}
		}

		String inputPath = inputPathTextField.getText();
		String outputPath = outputPathTextField.getText();
		if ((!inputPath.isEmpty()) && (!outputPath.isEmpty())) {
			String profArgs[] = profile.createLaunchArgs();
			String completeArgs[] = new String[profArgs.length + 2];
			completeArgs[0] = new File(inputPath).getPath();
			System.arraycopy(profArgs, 0, completeArgs, 1, profArgs.length);
			completeArgs[completeArgs.length - 1] = new File(outputPath).getPath();

			System.out.print("Complete arguments:");
			for (String arg : completeArgs)
				System.out.print(" " + arg);
			System.out.println("");
			try {
				ModifiedOverlayPDF.main(completeArgs);
				statusLabel.setText(
						String.format(GUIStrings.mainWindow.successfullOverlay, Paths.get(outputPath).getFileName()));
				statusLabel.setTextFill(Color.LIMEGREEN);
				successfullOutputFile = new File(outputPath);
				openFile.setVisible(true);
				openFileFolder.setVisible(true);
			} catch (IOException e) {
				e.printStackTrace();
				statusLabel.setText(GUIStrings.mainWindow.errOverlyingPDFS + e.getMessage());
				statusLabel.setTextFill(Paint.valueOf("RED"));
			}
		} else {
			statusLabel.setText(GUIStrings.mainWindow.errNoPathsGiven);
			statusLabel.setTextFill(Paint.valueOf("RED"));
		}

	}

	void reselectProfile() {
		int selIndex = profileListView.getSelectionModel().getSelectedIndex();
		profileListView.getSelectionModel().select(-1);
		profileListView.getSelectionModel().select(selIndex);
	}

	@SuppressWarnings("unchecked")
	void writeOutConfigFile() {
		File dataFile = new File(getDataDirectory() + "pdf-overlayer_saved-data.json");

		JSONObject data = new JSONObject();
		data.put(licenseShownKey, licenseShown);

		JSONArray fileArr = new JSONArray();
		for (OverlayProfile profile : profileList)
			fileArr.add(profile.getAsJSON());

		data.put(profilesKey, fileArr);

		try (FileWriter fw = new FileWriter(dataFile)) {
			data.writeJSONString(fw);
		} catch (IOException e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR, GUIStrings.mainWindow.errWritingSaveFile);
			alert.initOwner(primaryStage);
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.showAndWait();
		}
	}
	
	private void openAboutWindow() {
		new AboutWindow(this);
	}
}

class Tuple<A, B> {
	public A firstField;
	public B secondField;
}