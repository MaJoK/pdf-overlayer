/*  PDF-Overlayer, a GUI application utilizing JavaFx to overlay several PDF Files
    Copyright (C) 2018  Mark Joachim Krallmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.gitlab.majok.pdfOverlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gitlab.majok.pdfOverlayer.OverlayProfile.PDFDocument;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ProfileEditor {

	private static final int filePathFieldColumn = 0, fileChooserButtonColumn = 1, removeFileButtonColumn = 2,
			filePagesSeperatorColumn = 3, explicitPagesFieldColumn = 4, excPagesRadioButtonsSeperator = 5,
			firstButtonColumn = 6, lastButtonColumn = 7, evenButtonColumn = 8, oddButtonColumn = 9;
	/**
	 * Index of the row of where the labels describing the columns sit, and the
	 * index of the first row of the rows to configure the files
	 */
	private static final int columnLabelRow = 0, fileRowsStartingRow = 1;

	private static final double seperatorWidth = 50;

	private PDFOverlayerEntry mainWindowEntry;

	// The profile we are working with
	private OverlayProfile profile;
	private boolean newProfile;

	// VBox to put the top, middle and bottom pane into
	private VBox mainBox;
	private Stage ownStage;

	// The GridPane the middlePane places it's rows representing files into, as well
	// as the labels describing the columns
	private GridPane fileRowGP;
	private Button addFileRowButton;

	// The textfield the User enters the name of the profile into
	private TextField profileNameField;

	// Label to show warnings and errors on. It will be placed between the
	// middlepane and the bottom pane.
	private Label errorWarningLabel;

	private RadioButton backgroundButton;
	private RadioButton foregroundButton;

	// Textfield the User can enter a standard out directory in
	private TextField standardOut;

	// Labels in the columnlabels row
	private Label fileLabel, explicitPagesLabel;
	private static final int fileLabelColumnSpan = 3;
	private Label firstLabel, lastLabel, evenLabel, oddLabel;

	// File components lists
	private ArrayList<TextField> filePathFields;
	private ArrayList<Button> fileChooserButtons;
	private ArrayList<Button> removeFileRowButtons;
	// The user should enter the page numbers of the file seperated by comma or
	// spaces.
	private ArrayList<TextField> explicitPagesFields;

	// These Lists have listeners which add and remove the RadioButtons to
	// the ToggleGroups
	private ObservableList<RadioButton> firstRadioButtons;
	private ObservableList<RadioButton> lastRadioButtons;
	private ObservableList<RadioButton> evenRadioButtons;
	private ObservableList<RadioButton> oddRadioButtons;

	private ToggleGroup firstToggleGroup;
	private ToggleGroup lastToggleGroup;
	private ToggleGroup evenToggleGroup;
	private ToggleGroup oddToggleGroup;
	private TextField standardIn;

	public ProfileEditor(OverlayProfile profile, PDFOverlayerEntry mainWindowEntry) {
		this.mainWindowEntry = mainWindowEntry;
		this.profile = profile;
		newProfile = false;
		initProfileEditor();
	}

	public ProfileEditor(PDFOverlayerEntry mainWindowEntry) {
		this.mainWindowEntry = mainWindowEntry;
		profile = new OverlayProfile();
		newProfile = true;
		initProfileEditor();
	}

	/**
	 * Builds and set ups the stage
	 */
	private void initProfileEditor() {
		// Init the stage
		ownStage = new Stage();
		ownStage.initOwner(mainWindowEntry.primaryStage);
		ownStage.initModality(Modality.WINDOW_MODAL);
		ownStage.setResizable(false);
		ownStage.setTitle(GUIStrings.profileEditor.profileEditorWindowTitle);
		initFileComponentsLists();
		mainBox = new VBox();
		mainBox.setPadding(new Insets(10));
		mainBox.setSpacing(15);
		ownStage.setScene(new Scene(mainBox));

		assembleTopPane();
		assembleMainPane();
		addErrorWarningLabel();
		assembleBotPane();

		if (!newProfile)
			loadProfileIntoUI();

		if (newProfile || filePathFields.size() == 0) {
			addFileRowComponents();
			layOutFileComponents();
		}
		ownStage.show();

	}

	private void addErrorWarningLabel() {
		errorWarningLabel = new Label();
		errorWarningLabel.setWrapText(true);
		errorWarningLabel.setTextFill(Paint.valueOf("RED"));
		mainBox.getChildren().add(errorWarningLabel);

	}

	private void loadProfileIntoUI() {
		// Loading an empty profile could lead to errors and is unnecessary
		if (newProfile)
			return;
		// Adding the already present files
		for (int i = 0; i < profile.documents.length; i++) {
			PDFDocument doc = profile.documents[i];

			// Adding new row for this document
			addFileRowComponents();

			// Set the path of the pathField of our newly added row to the file's path
			TextField pathField = filePathFields.get(i);
			pathField.setText(doc.file.getPath());
			pathField.positionCaret(pathField.getLength() - 1);
			if (doc.explicitPages.length > 0) {
				StringBuilder sb = new StringBuilder();
				sb.append(String.valueOf(doc.explicitPages[0]));
				// Adding the rest of the pages with a comma and a space infront
				for (int o = 1; o < doc.explicitPages.length; o++) {
					sb.append(", ");
					sb.append(String.valueOf(doc.explicitPages[o]));
				}
				explicitPagesFields.get(i).setText(sb.toString());
			}
		}
		layOutFileComponents();

		// Setting background and profile name
		profileNameField.setText(profile.name);

		backgroundButton.setSelected(!profile.foreground);

		// Ensuring that the index of the default document is valid
		if (profile.indexOfDefault >= profile.documents.length) {
			System.err.println("indexOfDefault of the profile is invalid(too large), changing it to 0");
			profile.indexOfDefault = 0;
		}

		// Setting the RadioButtons according to the profile

		if (profile.first != -1 && profile.first < firstRadioButtons.size())
			firstRadioButtons.get(profile.first).setSelected(true);
		else
			firstRadioButtons.get(profile.indexOfDefault).setSelected(true);

		if (profile.last != -1 && profile.last < lastRadioButtons.size())
			lastRadioButtons.get(profile.last).setSelected(true);
		else
			lastRadioButtons.get(profile.indexOfDefault).setSelected(true);

		if (profile.even != -1 && profile.even < evenRadioButtons.size())
			evenRadioButtons.get(profile.even).setSelected(true);
		else
			evenRadioButtons.get(profile.indexOfDefault).setSelected(true);

		if (profile.odd != -1 && profile.odd < oddRadioButtons.size())
			oddRadioButtons.get(profile.odd).setSelected(true);
		else
			oddRadioButtons.get(profile.indexOfDefault).setSelected(true);

		if (profile.standardOut != null)
			standardOut.setText(profile.standardOut);
		if (profile.standardIn != null)
			standardIn.setText(profile.standardIn);
	}

	/**
	 * Writes the configuration done in the ui to the profile field.
	 * 
	 * @return whether the information of the ui has been written into the profile
	 *         field
	 */
	private boolean saveProfile() {
		// To avoid that the user clicks any more buttons
		mainBox.setDisable(true);

		// Checking whether a profile name has been entered
		if (profileNameField.getText().isEmpty()) {
			errorWarningLabel.setText(GUIStrings.profileEditor.errNoProfileNameEntered);
			mainBox.setDisable(false);
			return false;
		}

		// First checking for errors
		if (filePathFields.size() == 0) {
			errorWarningLabel.setText(GUIStrings.profileEditor.errNoFilesAdded);
			mainBox.setDisable(false);
			return false;
		}

		// Checking whether one of the filepath fields is empty
		for (TextField tf : filePathFields) {
			if (tf.getText().isEmpty()) {
				errorWarningLabel.setText(GUIStrings.profileEditor.errMinOneFilePathIsEmpty);
				mainBox.setDisable(false);
				return false;
			}
		}

		int firstIndex = firstRadioButtons.indexOf(firstToggleGroup.getSelectedToggle());
		int lastIndex = lastRadioButtons.indexOf(lastToggleGroup.getSelectedToggle());
		int evenIndex = evenRadioButtons.indexOf(evenToggleGroup.getSelectedToggle());
		int oddIndex = oddRadioButtons.indexOf(oddToggleGroup.getSelectedToggle());

		// Checking whether one of the buttons is somehow not selected
		if (firstIndex == -1 || lastIndex == -1 || evenIndex == -1 || oddIndex == -1) {
			errorWarningLabel.setText(
					"Error: one of the RadioButtons isn't selected! Check standard error stream for more information");
			System.err.println("firstIndex = " + firstIndex + " lastIndex = " + lastIndex + " evenIndex = " + evenIndex
					+ " oddIndex = " + oddIndex);
			mainBox.setDisable(false);
			return false;
		}

		int explicitPages[][];
		{
			// Parsing the entered pages of the explicit pages textfields and storing them
			// in explicitPages afterwards
			ArrayList<int[]> allPages = new ArrayList<int[]>(explicitPagesFields.size());
			for (TextField pageTF : explicitPagesFields) {
				if (!pageTF.getText().isEmpty()) {
					String[] seperatedPages = pageTF.getText().split("[, ]");
					ArrayList<String> singlePages = new ArrayList<String>(seperatedPages.length);

					for (String page : seperatedPages) {
						if (!page.isEmpty())
							singlePages.add(page);
					}

					int pages[] = new int[singlePages.size()];

					try {
						for (int i = 0; i < pages.length; i++) {
							pages[i] = Integer.parseInt(singlePages.get(i));
						}
					} catch (NumberFormatException e) {
						errorWarningLabel.setText(GUIStrings.profileEditor.errInvilidPagesEntered);
						mainBox.setDisable(false);
						return false;
					}

					allPages.add(pages);
				} else {
					allPages.add(new int[0]);
				}
			}

			explicitPages = allPages.toArray(new int[explicitPagesFields.size()][]);
		}

		// No problems present

		profile.name = profileNameField.getText();

		profile.documents = new PDFDocument[explicitPagesFields.size()];

		for (int i = 0; i < explicitPagesFields.size(); i++) {
			profile.documents[i] = new PDFDocument(new File(filePathFields.get(i).getText()), explicitPages[i]);
		}

		// We will choose the document selected as even document as the default document
		profile.indexOfDefault = evenIndex;

		profile.first = firstIndex != evenIndex ? firstIndex : -1;
		profile.last = lastIndex != evenIndex ? lastIndex : -1;
		profile.odd = oddIndex != evenIndex ? oddIndex : -1;
		profile.even = -1;

		profile.foreground = foregroundButton.isSelected();

		profile.useAllPages = false;

		profile.standardOut = standardOut.getText().isEmpty() ? null : standardOut.getText();

		profile.standardIn = standardIn.getText().isEmpty() ? null : standardIn.getText();
		mainBox.setDisable(false);
		return true;
	}

	private void assembleBotPane() {
		HBox botPane = new HBox();
		botPane.setAlignment(Pos.CENTER);
		botPane.setSpacing(30);

		Button saveButton = new Button(GUIStrings.profileEditor.saveButtonText);
		saveButton.setOnAction(ae -> {
			if (saveProfile()) {
				ownStage.hide();
				if (newProfile)
					mainWindowEntry.addProfile(profile);
				else {
					mainWindowEntry.writeOutConfigFile();
					mainWindowEntry.reselectProfile();
				}
			}
		});

		Button cancelButton = new Button(GUIStrings.profileEditor.cancelButtonText);
		cancelButton.setOnAction(ae -> {
			ownStage.hide();
		});
		botPane.getChildren().addAll(saveButton, cancelButton);

		mainBox.getChildren().add(botPane);
	}

	private void assembleTopPane() {
		HBox topPane = new HBox();
		topPane.setSpacing(10);
		Label profileNameLabel = new Label(GUIStrings.profileEditor.profileNameLabel);
		profileNameField = new TextField();
		topPane.setAlignment(Pos.CENTER_LEFT);

		// Box for the background or foreground selection
		VBox bgFgSelection = new VBox();
		bgFgSelection.setSpacing(5);
		Label bgFgLabel = new Label(GUIStrings.profileEditor.fgOrBgLabel);
		foregroundButton = new RadioButton(GUIStrings.profileEditor.foregroundOption);
		backgroundButton = new RadioButton(GUIStrings.profileEditor.backgroundOption);
		ToggleGroup group = new ToggleGroup();

		foregroundButton.setToggleGroup(group);
		backgroundButton.setToggleGroup(group);

		backgroundButton.setSelected(true);

		bgFgSelection.getChildren().addAll(bgFgLabel, foregroundButton, backgroundButton);
		VBox.setMargin(backgroundButton, new Insets(0, 0, 0, 10));
		VBox.setMargin(foregroundButton, new Insets(0, 0, 0, 10));

		topPane.getChildren().addAll(profileNameLabel, profileNameField, bgFgSelection);

		// Standard output textfield
		standardOut = new TextField();
		Button standardOutFileChoose = new Button("...");
		standardOutFileChoose.setOnAction(actionEvent -> {
			DirectoryChooser dc = new DirectoryChooser();
			File selected = dc.showDialog(ownStage);
			if (selected != null) {
				standardOut.setText(selected.getPath());
				standardOut.positionCaret(standardOut.getLength());
			}
		});
		Label standardOutLabel = new Label(GUIStrings.profileEditor.standardOutputDir);

		HBox standardOutHB = new HBox(standardOut, standardOutFileChoose);

		// Standard output textfield
		standardIn = new TextField();
		Button standardInFileChoose = new Button("...");
		standardInFileChoose.setOnAction(actionEvent -> {
			DirectoryChooser dc = new DirectoryChooser();
			File selected = dc.showDialog(ownStage);
			if (selected != null) {
				standardIn.setText(selected.getPath());
				standardIn.positionCaret(standardIn.getLength());
			}
		});
		Label standardInLabel = new Label(GUIStrings.profileEditor.standardInputDir);

		HBox standardInHB = new HBox(standardIn, standardInFileChoose);

		VBox standardTopVB = new VBox(5, standardOutLabel, standardOutHB, standardInLabel, standardInHB);
		HBox.setHgrow(standardOut, Priority.SOMETIMES);
		HBox.setHgrow(standardIn, Priority.SOMETIMES);
		Region spacingRegion = new Region();

		topPane.getChildren().addAll(spacingRegion, standardTopVB);

		HBox.setHgrow(spacingRegion, Priority.SOMETIMES);

		mainBox.getChildren().add(topPane);
	}

	private void assembleMainPane() {
		fileRowGP = new GridPane();

		mainBox.getChildren().add(fileRowGP);

		fileRowGP.setVgap(10);
		fileRowGP.setHgap(5);
		// Making the seperating spacing
		ColumnConstraints seperatorColumn = new ColumnConstraints(seperatorWidth);
		ObservableList<ColumnConstraints> cc = fileRowGP.getColumnConstraints();

		// We need to make sure that the list is big enough or we will get an out of
		// bounds exception.
		int biggerIndex = Math.max(filePagesSeperatorColumn, excPagesRadioButtonsSeperator);

		if (biggerIndex >= cc.size()) {
			for (int i = Math.max(0, cc.size() - 1); i <= biggerIndex; i++) {
				cc.add(new ColumnConstraints());
			}
		}
		cc.set(filePagesSeperatorColumn, seperatorColumn);
		cc.set(excPagesRadioButtonsSeperator, seperatorColumn);

		// Creating the column Labels

		fileLabel = new Label(GUIStrings.profileEditor.filesLabel);
		explicitPagesLabel = new Label(GUIStrings.profileEditor.explicitPagesLabel);

		firstLabel = new Label(GUIStrings.profileEditor.firstLabel);
		lastLabel = new Label(GUIStrings.profileEditor.lastLabel);
		evenLabel = new Label(GUIStrings.profileEditor.evenLabel);
		oddLabel = new Label(GUIStrings.profileEditor.oddLabel);

		fileRowGP.add(fileLabel, filePathFieldColumn, columnLabelRow, fileLabelColumnSpan, 1);
		fileRowGP.add(explicitPagesLabel, explicitPagesFieldColumn, columnLabelRow);

		fileRowGP.add(firstLabel, firstButtonColumn, columnLabelRow);
		fileRowGP.add(lastLabel, lastButtonColumn, columnLabelRow);
		fileRowGP.add(evenLabel, evenButtonColumn, columnLabelRow);
		fileRowGP.add(oddLabel, oddButtonColumn, columnLabelRow);

		addFileRowButton = new Button(GUIStrings.profileEditor.addFile);
		addFileRowButton.setOnAction(ae -> {
			addFileRowComponents();
			layOutFileComponents();
		});
		fileRowGP.add(addFileRowButton, filePathFieldColumn, fileRowsStartingRow + 1);
		layOutFileComponents();
	}

	/**
	 * Adds the components of one file row to the lists.
	 * {@link #layOutFileComponents()} needs to be called afterwards to show the
	 * components
	 */
	private void addFileRowComponents() {

		TextField filePathField = new TextField();
		filePathFields.add(filePathField);

		Button fileChooseButton = mainWindowEntry.getFileOpenButton(filePathField, ownStage);
		fileChooserButtons.add(fileChooseButton);

		Button rowDelete = new Button("X");
		removeFileRowButtons.add(rowDelete);

		TextField explicitPagesField = new TextField();
		explicitPagesFields.add(explicitPagesField);

		RadioButton first = new RadioButton("");
		RadioButton even = new RadioButton("");
		RadioButton odd = new RadioButton("");
		RadioButton last = new RadioButton("");

		first.setAlignment(Pos.CENTER);
		last.setAlignment(Pos.CENTER);
		even.setAlignment(Pos.CENTER);
		odd.setAlignment(Pos.CENTER);

		firstRadioButtons.add(first);
		lastRadioButtons.add(last);
		evenRadioButtons.add(even);
		oddRadioButtons.add(odd);

		rowDelete.setOnAction(ae -> {
			int rowIndex = removeFileRowButtons.indexOf(rowDelete);
			if (rowIndex != -1) {
				removeFileRow(rowIndex);
				layOutFileComponents();
			}
		});
		fileRowGP.getChildren().addAll(filePathField, fileChooseButton, rowDelete, explicitPagesField, first, last, odd,
				even);
	}

	private void removeFileRow(final int rowIndex) {
		ObservableList<Node> children = fileRowGP.getChildren();
		// @formatter:off
		children.removeAll(filePathFields.remove(rowIndex), fileChooserButtons.remove(rowIndex),
				removeFileRowButtons.remove(rowIndex), explicitPagesFields.remove(rowIndex),

				firstRadioButtons.remove(rowIndex), lastRadioButtons.remove(rowIndex),
				evenRadioButtons.remove(rowIndex), oddRadioButtons.remove(rowIndex));
		// @formatter:on

	}

	/**
	 * Puts the file components at the right position in the fileRowGP
	 */
	private void layOutFileComponents() {

		setCorrectConstraints(filePathFieldColumn, filePathFields);
		setCorrectConstraints(fileChooserButtonColumn, fileChooserButtons);
		setCorrectConstraints(removeFileButtonColumn, removeFileRowButtons);
		setCorrectConstraints(explicitPagesFieldColumn, explicitPagesFields);

		setCorrectConstraints(firstButtonColumn, firstRadioButtons);
		setCorrectConstraints(lastButtonColumn, lastRadioButtons);
		setCorrectConstraints(evenButtonColumn, evenRadioButtons);
		setCorrectConstraints(oddButtonColumn, oddRadioButtons);

		GridPane.setConstraints(addFileRowButton, filePathFieldColumn, filePathFields.size() + fileRowsStartingRow);

		ownStage.sizeToScene();
	}

	private void setCorrectConstraints(int column, List<? extends Node> list) {
		Iterator<? extends Node> it = list.iterator();
		int index = fileRowsStartingRow;
		while (it.hasNext()) {
			GridPane.setConstraints(it.next(), column, index++);
		}
	}

	/**
	 * Inits the file components lists
	 */
	private void initFileComponentsLists() {
		filePathFields = new ArrayList<TextField>();
		fileChooserButtons = new ArrayList<Button>();
		removeFileRowButtons = new ArrayList<Button>();
		explicitPagesFields = new ArrayList<TextField>();

		firstRadioButtons = FXCollections.observableArrayList();
		firstRadioButtons.addListener(new RadioButtonListListener(firstToggleGroup = new ToggleGroup()));

		lastRadioButtons = FXCollections.observableArrayList();
		lastRadioButtons.addListener(new RadioButtonListListener(lastToggleGroup = new ToggleGroup()));

		evenRadioButtons = FXCollections.observableArrayList();
		evenRadioButtons.addListener(new RadioButtonListListener(evenToggleGroup = new ToggleGroup()));

		oddRadioButtons = FXCollections.observableArrayList();
		oddRadioButtons.addListener(new RadioButtonListListener(oddToggleGroup = new ToggleGroup()));

	}

	private class RadioButtonListListener implements ListChangeListener<RadioButton> {
		ToggleGroup group;

		public RadioButtonListListener(ToggleGroup group) {
			this.group = group;
		}

		@Override
		public void onChanged(Change<? extends RadioButton> c) {

			while (c.next()) {
				if (c.wasAdded()) {
					for (RadioButton added : c.getAddedSubList()) {
						added.setToggleGroup(group);
					}
				} else if (c.wasRemoved()) {
					for (RadioButton removed : c.getRemoved())
						removed.setToggleGroup(null);

				}
				// no selected toggle, there should always be at least one
				if (group.getSelectedToggle() == null) {
					List<Toggle> toggles = group.getToggles();
					if (toggles.size() > 0)
						group.selectToggle(toggles.get(0));
				}
			}
		}

	}
}