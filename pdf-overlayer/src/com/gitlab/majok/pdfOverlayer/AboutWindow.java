/*  PDF-Overlayer, a GUI application utilizing JavaFx to overlay several PDF Files
    Copyright (C) 2018  Mark Joachim Krallmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.gitlab.majok.pdfOverlayer;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AboutWindow extends Stage {
	private static final String freeSoftwareNotice = "PDF-Overlayer, a GUI application utilizing JavaFx to overlay several PDF Files"
			+ "\nCopyright (C) 2018 Mark Joachim Krallmann\n" + "This program comes with ABSOLUTELY NO WARRANTY;\n"
			+ "This is free software, and you are welcome to redistribute it under certain conditions;\n"
			+ "See the Full License for details.";
	private static final URI sourceURI = URI.create("https://gitlab.com/MaJoK/pdf-overlayer");
	private static final URI licenseURI = URI.create("https://www.gnu.org/licenses/gpl.html");
	private static final URI mailURI = URI.create("mailto:joachim.krallmann@protonmail.com");

	GridPane topLevel;

	AboutWindow(PDFOverlayerEntry entry) {
		super();
		initOwner(entry.primaryStage);
		initModality(Modality.WINDOW_MODAL);

		topLevel = new GridPane();
		topLevel.setVgap(10);
		topLevel.setHgap(10);
		topLevel.setPadding(new Insets(20));
		addFreeSoftwareNotice();
		addHyperLinks();
		Button close = new Button(GUIStrings.about.closeButton);
		close.setOnAction(ae -> this.hide());

		topLevel.add(close, 0, 2, 3, 1);
		ObservableList<ColumnConstraints> constraints = topLevel.getColumnConstraints();
		ColumnConstraints growCenter = new ColumnConstraints();
		growCenter.setHgrow(Priority.SOMETIMES);
		growCenter.setMaxWidth(Double.MAX_VALUE);
		growCenter.setHalignment(HPos.CENTER);
		constraints.addAll(growCenter, growCenter, growCenter);
		GridPane.setHalignment(close, HPos.CENTER);
		setScene(new Scene(topLevel));
		show();
	}

	private void addFreeSoftwareNotice() {
		Text notice = new Text(freeSoftwareNotice);
		notice.setTextAlignment(TextAlignment.CENTER);
		topLevel.add(notice, 0, 0, 3, 1);

	}

	private void addHyperLinks() {
		Desktop.isDesktopSupported();// Might help to avoid errors
		Hyperlink sourceCode = new Hyperlink("Source Code");
		sourceCode.setOnAction(actionEvent -> {
			new Thread(() -> {
				try {
					Desktop.getDesktop().browse(sourceURI);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}).start();
		});
		Hyperlink fullLicense = new Hyperlink("Full License");
		fullLicense.setOnAction(actionEvent -> {

			new Thread(() -> {
				try {
					Desktop.getDesktop().browse(licenseURI);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}).start();
		});

		Hyperlink contactAuthor = new Hyperlink("Contact the Author");
		contactAuthor.setOnAction(actionEvent -> {
			new Thread(() -> {
				try {
					Desktop.getDesktop().browse(mailURI);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}).start();

		});

		topLevel.addRow(1, sourceCode, fullLicense, contactAuthor);
	}
}
