/*  PDF-Overlayer, a GUI application utilizing JavaFx to overlay several PDF Files
    Copyright (C) 2018  Mark Joachim Krallmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.gitlab.majok.pdfOverlayer.modifedOverlayPDFTool;

/* 
* PDF-Overlayer is a program to overlay pdf files using a gui
* Copyright (C) <2018>  <Mark Joachim Krallmann>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
* 
* 
* The original Version of this file is the org.apache.pdfbox.tools.OverlayPDF 
* class found in the Apache PDFBox library (https://pdfbox.apache.org/) 
* and is licensed under the Apache 2 License (https://www.apache.org/licenses/LICENSE-2.0.html)
* 
*/
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.multipdf.Overlay.Position;
import org.apache.pdfbox.pdmodel.PDDocument;

/**
 * 
 * Adds an overlay to an existing PDF document.
 * 
 * Based on code contributed by Balazs Jerk.
 * 
 */
public final class ModifiedOverlayPDF {
	private static final Log LOG = LogFactory.getLog(ModifiedOverlayPDF.class);

	private static final String POSITION = "-position";
	private static final String ODD = "-odd";
	private static final String EVEN = "-even";
	private static final String FIRST = "-first";
	private static final String LAST = "-last";
	private static final String PAGE = "-page";
	private static final String USEALLPAGES = "-useAllPages";

	private ModifiedOverlayPDF() {
	}

	/**
	 * This will overlay a document and write out the results.
	 *
	 * @param args command line arguments
	 * @throws IOException if something went wrong
	 */
	public static void main(final String[] args) throws IOException {

		String outputFilename = null;
		Overlay overlayer = new Overlay();
		Map<Integer, String> specificPageOverlayFile = new HashMap<Integer, String>();
		// input arguments
		for (int i = 0; i < args.length; i++) {
			String arg = args[i].trim();
			if (i == 0) {
				overlayer.setInputFile(arg);
			} else if (i == (args.length - 1)) {
				outputFilename = arg;
			} else if (arg.equals(POSITION) && ((i + 1) < args.length)) {
				if (Position.FOREGROUND.toString().equalsIgnoreCase(args[i + 1].trim())) {
					overlayer.setOverlayPosition(Position.FOREGROUND);
				} else if (Position.BACKGROUND.toString().equalsIgnoreCase(args[i + 1].trim())) {
					overlayer.setOverlayPosition(Position.BACKGROUND);
				} else {
					System.err.println("Error while parsing the Position argument");
				}
				i += 1;
			} else if (arg.equals(ODD) && ((i + 1) < args.length)) {
				overlayer.setOddPageOverlayFile(args[i + 1].trim());
				i += 1;
			} else if (arg.equals(EVEN) && ((i + 1) < args.length)) {
				overlayer.setEvenPageOverlayFile(args[i + 1].trim());
				i += 1;
			} else if (arg.equals(FIRST) && ((i + 1) < args.length)) {
				overlayer.setFirstPageOverlayFile(args[i + 1].trim());
				i += 1;
			} else if (arg.equals(LAST) && ((i + 1) < args.length)) {
				overlayer.setLastPageOverlayFile(args[i + 1].trim());
				i += 1;
			} else if (arg.equals(USEALLPAGES) && ((i + 1) < args.length)) {
				overlayer.setAllPagesOverlayFile(args[i + 1].trim());
				i += 1;
			} else if (arg.equals(PAGE) && ((i + 2) < args.length) && (isInteger(args[i + 1].trim()))) {
				specificPageOverlayFile.put(Integer.parseInt(args[i + 1].trim()), args[i + 2].trim());
				i += 2;
			} else if (overlayer.getDefaultOverlayFile() == null) {
				overlayer.setDefaultOverlayFile(arg);
			} else {
				System.err.println("Unknown Argument: \"" + arg + "\"");
				return;
			}
		}

		if (overlayer.getInputFile() == null || outputFilename == null) {
			System.err.println("No input file or output file found.");
			return;
		}

		try {
			PDDocument result = overlayer.overlay(specificPageOverlayFile);
			result.save(outputFilename);
			result.close();
			// close the input files AFTER saving the resulting file as some
			// streams are shared among the input and the output files
			overlayer.close();
		} catch (IOException e) {
			LOG.error("Overlay failed: " + e.getMessage(), e);
			throw e;
		}
	}

	private static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}
