/*  PDF-Overlayer, a GUI application utilizing JavaFx to overlay several PDF Files
    Copyright (C) 2018  Mark Joachim Krallmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.gitlab.majok.pdfOverlayer;

/**
 * This class stores Strings to use in the GUI, later these will be loaded
 * according to the locale.
 *
 */
public class GUIStrings {

	private GUIStrings() {
	}

	public static String fileChooserPDFFilesExtensionLabel = "PDF files",
			fileChooserLoadFileTitle = "Select the PDF file to overlay",
			fileChooserSaveFileTitle = "Select file to save into";

	public static class mainWindow {
		public static final String about = "About";

		public static String errNoPathsGiven = "No input file and/or output file specified.",
				errWritingSaveFile = "Error while trying to write to save file!",
				errOverlyingPDFS = "An error occured while trying to overlay the pdf files: ";

		public static String warnWillOverwriteInputFile = "Continuing will overwrite the inputfile!";
		public static String startProcess = "Go", successfullOverlay = "Successfully overlayed the files into %s",
				errNeedToSelectProfile = "You need to select a profile";
		public static String mainWindowTitle = "PDF-Overlayer", openFileButton = "Open file",
				openFolderButton = "Open containing folder";
		public static final String newProfileButton = "Create a new profile",
				editProfileButton = "Edit selected profile", removeProfileButton = "Remove selected profile",
				suggestInputFile = "Suggest input file for profile",
				suggestInputFileTooltip = "If there is a directory configured when "
						+ "choosing the input file for this profile the program will "
						+ "suggest the latest modified pdf-file in that directory as input file.";
		public static final String conformationDeleteProfile = "Do you really want to delete the profile?";

	}

	/**
	 * @author mjk
	 *
	 */
	public static class profileEditor {
		public static final String standardInputDir = "Initial Directory when choosing the input file for this profile:";
		public static String profileEditorWindowTitle = "PDF-Overlayer: Profile Editor";
		public static String profileNameLabel = "Profile name:", oddLabel = "Odd pages", evenLabel = "Even pages",
				firstLabel = "First page", lastLabel = "Last page";
		public static String standardOutputDir = "Directory of suggested output file:";
		public static String filesLabel = "Files", addFile = "Add file", explicitPagesLabel = "Explicit pages";
		public static String enterPagesHere = "Pages to overlay this pdf explicitly on, (seperate by comma or space)";

		public static String saveButtonText = "Save profile", foregroundOption = "Foreground",
				backgroundOption = "Background", fgOrBgLabel = "Put overlays in", cancelButtonText = "Cancel";

		/**
		 * 
		 */
		public static String errNoFilesAdded = "You need add at least one file!",
				errNoProfileNameEntered = "No name was entered!";
		/**
		 * To use when at least one of the file path fields is empty
		 */
		public static String errMinOneFilePathIsEmpty = "At least one of the entered file paths is empty. Please remove all empty file rows with the X button or add files.";

		/**
		 * To use when the user entered an invalid String into the explicit pages field.
		 * E.G a letter(comma and space is allowed).
		 */
		public static String errInvilidPagesEntered = "Invalid Symbols entered into the page numbers field. Only numbers(0-9), spaces and commas are allowed!";

		public static class old {
			private old() {
			}

			public static String

			useAllPagesCheckBox = "Use all pages",
					useAllPagesTooltip = "If checked all pages of the document will be used by repeating them.";
		}
	}

	public static class about {
		private about() {
		}

		public static final String closeButton = "Close";
	}
}
