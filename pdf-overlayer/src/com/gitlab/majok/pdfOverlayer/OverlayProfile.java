/*  PDF-Overlayer, a GUI application utilizing JavaFx to overlay several PDF Files
    Copyright (C) 2018  Mark Joachim Krallmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.gitlab.majok.pdfOverlayer;

import java.io.File;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class OverlayProfile {

	/**
	 * The name shown in the profile list
	 */
	String name;

	/**
	 * The folder to propose as output folder for the selected input file
	 */
	String standardOut;
	/**
	 * The initial Directory when letting the User choose an input file for this
	 * profile
	 */
	String standardIn;

	/**
	 * The documents used in this profile to overlay a pdf file
	 */
	PDFDocument documents[];

	int odd, even, first, last, indexOfDefault;
	boolean useAllPages, foreground;

	// Keys to store and retrieve the fields using JSON
	static final String nameKey = "name", documentsKey = "files", filePathKey = "path", oddKey = "indexOfOdd",
			evenKey = "indexOfEven", firstKey = "indexOfFirst", lastKey = "indexOfLast", defaultKey = "indexOfDefault",
			useAllPagesKey = "useAllPages", foregroundKey = "putInForeground", pagesKey = "explicitPages",
			standardOutKey = "standardOutputFolder", standardInKey = "standardInputFolder";

	/**
	 * Constructs an empty OverlayProfile
	 */
	public OverlayProfile() {
		odd = -1;
		even = -1;
		first = -1;
		last = -1;
		indexOfDefault = -1;

		useAllPages = false;
		foreground = false;
		documents = new PDFDocument[0];

		name = "";
	}

	/**
	 * Constructs an empty OverlayProfile and sets the name.
	 * 
	 * @param name The string to set the name to
	 */
	public OverlayProfile(String name) {
		this.name = name;
		odd = -1;
		even = -1;
		first = -1;
		last = -1;
		indexOfDefault = -1;

		useAllPages = false;
		foreground = false;
		documents = new PDFDocument[0];
	}

	/**
	 * Retrieves the stored profile information and stores them in the fields of
	 * this object
	 * 
	 * @param o The JSONObject which contains the fields of this profile
	 * @throws ConstructException If any error or exception occured while trying to
	 *                            read the fields. The stored data is most likely
	 *                            damaged and garbage if this occures.
	 * 
	 */
	public OverlayProfile(JSONObject o) throws ConstructException {
		try {
			name = (String) o.get(nameKey);
			if (name == null)
				throw new ConstructException("Key " + nameKey + " not found");
			JSONArray files = (JSONArray) o.get(documentsKey);
			documents = new PDFDocument[files.size()];

			for (int i = 0; i < files.size(); i++) {
				JSONObject doc = (JSONObject) files.get(i);
				String path = (String) (doc).get(filePathKey);
				JSONArray jspages = (JSONArray) doc.get(pagesKey);
				int pages[] = new int[jspages.size()];
				for (int j = 0; j < pages.length; j++) {
					long temp = (Long) jspages.get(j);
					pages[j] = (int) temp;
				}
				documents[i] = new PDFDocument(new File(path), pages);
			}

			Long _odd = (Long) o.get(oddKey);
			odd = (int) (_odd != null ? _odd : -1);

			Long _first = (Long) o.get(firstKey);
			first = (int) (_first != null ? _first : -1);

			Long _last = (Long) o.get(lastKey);
			last = (int) (_last != null ? _last : -1);

			long _indexOfDefault = (Long) o.get(defaultKey);
			indexOfDefault = (int) _indexOfDefault;

			Long _even = (Long) o.get(evenKey);
			even = (int) (_even != null ? _even : -1);

			useAllPages = (boolean) o.get(useAllPagesKey);
			foreground = (boolean) o.get(foregroundKey);

			if (o.containsKey(standardOutKey))
				standardOut = (String) o.get(standardOutKey);
			if (o.containsKey(standardInKey))
				standardIn = (String) o.get(standardInKey);

		} catch (ConstructException e) {
			throw e;
		} catch (Exception e) {
			throw new ConstructException(e);
		}

	}

	/**
	 * Creates args to pass to the ModifiedOverlayPDF class. Input and output files
	 * will have to placed before and after these args.
	 * <p>
	 * Check the {@link org.apache.pdfbox.tools.OverlayPDF} class for further
	 * information.
	 */
	String[] createLaunchArgs() {
		ArrayList<String> args = new ArrayList<String>(20);

		if (useAllPages)
			args.add("-useAllPages");

		args.add(documents[indexOfDefault].file.getPath());
		if (odd != -1) {
			args.add("-odd");
			args.add(documents[odd].file.getPath());
		}
		if (even != -1) {
			args.add("-even");
			args.add(documents[even].file.getPath());
		}
		if (first != -1) {
			args.add("-first");
			args.add(documents[first].file.getPath());
		}
		if (last != -1) {
			args.add("-last");
			args.add(documents[last].file.getPath());
		}
		args.add("-position");
		args.add(foreground ? "foreground" : "background");

		for (PDFDocument doc : documents) {
			for (int page : doc.explicitPages) {
				args.add("-page");
				args.add(String.valueOf(page));
				args.add(doc.file.getPath());
			}
		}

		return args.toArray(new String[0]);
	}

	/**
	 * Stored the fields of this instance inside an JSONObject and returns it for
	 * storage on a disk.
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {
		JSONObject o = new JSONObject();
		o.put(nameKey, name);
		o.put(oddKey, odd);
		o.put(evenKey, even);
		o.put(firstKey, first);
		o.put(lastKey, last);
		o.put(defaultKey, indexOfDefault);
		o.put(useAllPagesKey, useAllPages);
		o.put(foregroundKey, foreground);
		JSONArray docArray = new JSONArray();
		for (int i = 0; i < documents.length; i++) {
			JSONObject doc = new JSONObject();
			doc.put(filePathKey, documents[i].file.getPath());

			JSONArray pageArr = new JSONArray();
			for (int page : documents[i].explicitPages)
				pageArr.add(page);
			doc.put(pagesKey, pageArr);

			docArray.add(doc);

		}
		o.put(documentsKey, docArray);
		if (standardOut != null)
			o.put(standardOutKey, standardOut);
		if (standardIn != null)
			o.put(standardInKey, standardIn);

		return o;
	}

	public String getName() {
		return name;
	}

	public void setName(String newName) {
		name = newName;
	}

	@Override
	public String toString() {
		return name.isEmpty() ? super.toString() : name;
	}

	/**
	 * Represents a pdf document used to overlay files.
	 *
	 */
	public static class PDFDocument {
		File file;
		int explicitPages[];

		public PDFDocument(File file, int explicitPages[]) {
			this.file = file;
			this.explicitPages = explicitPages;
		}
	}

	/**
	 * Thrown when an error occures while trying to reconstruct an instance from an
	 * JSONObject.
	 *
	 */
	public class ConstructException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6817997068718776447L;

		public ConstructException() {
			super();
		}

		public ConstructException(String message, Throwable cause, boolean enableSuppression,
				boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}

		public ConstructException(String message, Throwable cause) {
			super(message, cause);
		}

		public ConstructException(String message) {
			super(message);
		}

		public ConstructException(Throwable cause) {
			super(cause);
		}

	}
}
