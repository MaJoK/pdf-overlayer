#!/bin/sh
set -e
mvn clean package
cp target/pdf-overlayer*.jar deb_package/usr/bin/mjk-pdf-overlayer/mjk-pdf-overlayer.jar
cp target/lib -r deb_package/usr/bin/mjk-pdf-overlayer/lib
dpkg -b deb_package/ mjk-pdf-overlayer.deb
