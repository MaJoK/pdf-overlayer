## pdf-overlayer
A small GUI program to overlay pdf files over each other.
It utilizes JavaFX to present the GUI and the Apache PDFBox iibrary to overlay 
the pdf files.

To allow quick usage profiles can be created and stored which store which file 
should be used for which pages.

## Building

To build it install Apache Maven and run  
"mvn clean package"  
in the project directory.
The jar file can be found in the target subdirectory and is called "pdf-overlayer-1.2.jar"

To run in it use "java --module-path=target/lib/ --add-modules=javafx.controls -jar target/pdf-overlayer-1.2.jar"